# easytls-consul-template ansible role

This is an alternative way of issueing TLS certificates from HashiCorp Vault. This could also be done with `community.hashi_vault` collection, 
but that collection requires you to run the ansible playbook periodically. In some environments this is not possible.

This role is based on [consul-template](https://github.com/hashicorp/consul-template) and will run as a service on the target machine as `easytls-consul-template.service`.

The service itself will take care of re-issuing certificates when they are about to expire.

## global variables used

These are best to put in `group_vars/all.yml`.

| name | default | description | type |
| ---- | -------- | ----------- | ---- |
| `easytls_vault_address` | `https://vault.service.consul:8200` | The Vault address | string |
| `easytls_vault_ssl_verify` | `false` | Verify Vault SSL certificate | string |
| `easytls_vault_token` | NO DEFAULT, mandatory | The Vault token used to issue certificates| string |
| `easytls_vault_role` | NO DEFAULT, mandatory | The Vault role  used to issue certificates| string |
| `easytls_certs_dir` | `/etc/easytls/certs` | The certs directory | string |
| `easytls_scripts_dir` | `/etc/easytls/scripts` | The reload scripts directory | string |

## host or group variables

Depending on your setup, these are best used in `group_vars/some_group.yml` or as `host_vars/some_host.yml`.

| name | default | description | type |
| ---- | -------- | ----------- | ---- |
| `easytls_common_name` | `"{{ ansible_facts['fqdn'] }}"` | The common_name of the certificate | string |
| `easytls_cert` | `"{{ ansible_facts['fqdn'] }}"` | The certificate name | string |
| `easytls_ttl` | `""` | The TTL of the certificate | string |
| `easytls_alt_names` | `[]` | The alt_names of the certificate | list |
| `easytls_ip_sans` | `[]` | The ip_sans of the certificate | list |
| `easytls_reload_commands` | `[]` | The reload commands | list |
