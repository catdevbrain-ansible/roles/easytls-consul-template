---

- name: Ensure hashicorp apt repository is present
  block:
    - name: Get hashicorp repository key
      ansible.builtin.get_url:
        url: https://apt.releases.hashicorp.com/gpg
        dest: /usr/share/keyrings/hashicorp-archive-keyring.gpg_armored

    - name: De-armor hashicorp repository key
      ansible.builtin.shell:
        cmd: gpg --dearmor < /usr/share/keyrings/hashicorp-archive-keyring.gpg_armored > /usr/share/keyrings/hashicorp-archive-keyring.gpg
        creates: /usr/share/keyrings/hashicorp-archive-keyring.gpg

    - name: Add hashicorp repository
      ansible.builtin.apt_repository:
        repo: "deb [arch=amd64 signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com bookworm main"
        filename: hashicorp
        state: present
  when: ansible_distribution == 'Debian'

- name: Ensure hashicorp yum repository is present
  ansible.builtin.yum_repository:
    name: hashicorp_stable
    description: Hashicorp repository
    state: present
    baseurl: https://rpm.releases.hashicorp.com/RHEL/$releasever/$basearch/stable
    gpgcheck: true
    gpgkey: https://rpm.releases.hashicorp.com/gpg
  when: ansible_distribution == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == 'AlmaLinux'

- name: Install easytls-consul-template for debian/ubuntu
  ansible.builtin.apt:
    name:
      - consul-template
    state: present
  when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

- name: Install easytls-consul-template for redhat
  ansible.builtin.dnf:
    name:
      - consul-template
    state: present
  when: ansible_distribution == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == 'AlmaLinux'

- name: Symlink easytls-consul-template to consul-template
  ansible.builtin.file:
    src: /usr/bin/consul-template
    dest: /usr/bin/easytls-consul-template
    state: link

- name: Create easytls-consul-template directories
  ansible.builtin.file:
    state: directory
    path: "{{ item }}"
    owner: root
    group: root
    mode: 0755
  with_items:
    - /etc/easytls
    - /etc/easytls/certs
    - /etc/easytls/scripts

- name: Install easytls-consul-template config
  ansible.builtin.template:
    src: easytls.hcl.j2
    dest: /etc/easytls/easytls.hcl
    owner: root
    group: root
    mode: 0640
  notify:
    - Restart easytls-consul-template.service

- name: Install easytls reload commands script
  ansible.builtin.template:
    src: reload_commands.sh.j2
    dest: /etc/easytls/scripts/reload_commands_{{ easytls_cert }}.sh
    owner: root
    group: root
    mode: 0755
  notify:
    - Restart easytls-consul-template.service

- name: Install easytls-consul-template systemd unit
  ansible.builtin.copy:
    src: easytls-consul-template.service
    dest: /etc/systemd/system/easytls-consul-template.service
    owner: root
    group: root
    mode: 0644

- name: Start easytls-consul-template.service
  ansible.builtin.service:
    name: easytls-consul-template.service
    state: started
    enabled: true
